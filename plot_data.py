#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script to plot figures from aggregation data 
#
# (C) Copyright 2018 Emma Fitzgerald, Artur Tomaszewski 
# emma.fitzgerald@eit.lth.se, artur@tele.pw.edu.pl
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams.update({'font.size': 18})
import matplotlib.pyplot as plt

data = np.genfromtxt("results/100_results.dat")

num_nodes = np.unique(data[:,0])

oneK_vectors = []
nK_vectors = []
for val in num_nodes:
    idx = data[:,0] == val 
    vec = data[np.where(idx),9]
    oneK_vectors.append(vec)
    vec = data[np.where(idx),10]
    nK_vectors.append(vec)

def routing_time_1K():
    fig, axes = plt.subplots()
    bp_1K = axes.boxplot(oneK_vectors)
    print [0] + num_nodes.tolist()
    plt.xticks(xrange(len(num_nodes) + 1), [0] + num_nodes.tolist())
    axes.set_ylim(ymin=0.005)
    axes.set_yscale("log")
    #axes.set_ylim(ymax=200)
    plt.xlabel("Number of nodes")
    plt.ylabel("Solution time [s]")
    plt.savefig("solution_time_routing_1K.eps")
    plt.cla()

def routing_time_nK():
    fig, axes = plt.subplots()
    bp_nK = axes.boxplot(nK_vectors)
    print [0] + num_nodes.tolist()
    plt.xticks(xrange(len(num_nodes) + 1), [0] + num_nodes.tolist())
    #plt.ylim([0, 20000])
    axes.set_ylim(ymin=0.005)
    axes.set_yscale("log")
    plt.xlabel("Number of nodes")
    plt.ylabel("Solution time [s]")
    plt.savefig("solution_time_routing_nK.eps")
    plt.cla()

def energy_time_scatter():
    energy = data[:,3]
    time = data[:,9]
    plt.scatter(energy, time)
    plt.show()

aggr_data = np.genfromtxt("aggregated_results.dat")

def energy_bars_1K():
    width = 3
    plt.xticks(aggr_data[:,0])
    # Transmission energy 1K
    p1 = plt.bar(aggr_data[:,0], aggr_data[:,5], width, color='#FF5555', yerr=aggr_data[:,6], align="center", ecolor="black",
                error_kw=dict(lw=2, capsize=2, capthick=2))
    # Aggregation energy 1K
    p2 = plt.bar(aggr_data[:,0], aggr_data[:,9], width, color="#5555FF", yerr=aggr_data[:,10], bottom=aggr_data[:,5], 
                    align="center", ecolor="black", error_kw=dict(lw=2, capsize=2, capthick=2))
    plt.legend((p1[0], p2[0]), ('Transmission', 'Aggregation'), loc="upper left")
    plt.ylabel("Energy cost")
    plt.xlabel("Number of nodes")
    plt.savefig("energy_costs_1K.eps")
    plt.cla()

def energy_bars_nK():
    width = 3
    plt.xticks(aggr_data[:,0])
    # Transmission energy 1K
    p1 = plt.bar(aggr_data[:,0], aggr_data[:,7], width, color='#FF5555', yerr=aggr_data[:,8], align="center", ecolor="black",
            error_kw=dict(lw=2, capsize=2, capthick=2))
    # Aggregation energy 1K
    p2 = plt.bar(aggr_data[:,0], aggr_data[:,11], width, color="#5555FF", yerr=aggr_data[:,12], bottom=aggr_data[:,7], 
                    align="center", ecolor="black", error_kw=dict(lw=2, capsize=2, capthick=2))
    plt.legend((p1[0], p2[0]), ('Transmission', 'Aggregation'), loc="upper left")
    plt.ylabel("Energy cost")
    plt.xlabel("Number of nodes")
    plt.savefig("energy_costs_nK.eps")
    plt.cla()

def num_aggregators_1K():
    width = 3
    plt.xticks(aggr_data[:,0])
    p1 = plt.bar(aggr_data[:,0], aggr_data[:,25], width, color='#FF5555', yerr=aggr_data[:,26], align="center", ecolor="black",
            error_kw=dict(lw=2, capsize=2, capthick=2))
    print aggr_data[:,25]
    plt.ylabel("Number of aggregators used")
    plt.xlabel("Number of nodes")
    plt.savefig("num_aggregators_1K.eps")
    plt.cla()

def num_aggregators_nK():
    width = 3
    plt.xticks(aggr_data[:,0])
    p1 = plt.bar(aggr_data[:,0], aggr_data[:,27], width, color='#FF5555', yerr=aggr_data[:,28], align="center", ecolor="black",
            error_kw=dict(lw=2, capsize=2, capthick=2))

    print aggr_data[:,27]
    plt.ylabel("Number of aggregators used")
    plt.xlabel("Number of nodes")
    plt.savefig("num_aggregators_nK.eps")
    plt.cla()

energy_bars_1K()
energy_bars_nK()
routing_time_1K()
routing_time_nK()
num_aggregators_1K()
num_aggregators_nK()

for line in aggr_data[:]:
    print str(line[0]) + " & " + str(round(line[13], 2)) + " ($\pm$" + str(round(line[14], 2)) + ") & " + str(round(line[19], 2)) + " ($\pm$" + str(round(line[20], 2)) + ") & ",
    print str(round(line[21], 2)) + " ($\pm$" + str(round(line[22], 2)) + ") & " + str(round(line[23], 2)) + " ($\pm$" + str(round(line[24], 2)) + ") \\\\"
    print "\hline"
