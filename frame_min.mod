# Minimise the frame length for 1K data aggregation: dual problem
#
# (C) Copyright 2018 Emma Fitzgerald, Artur Tomaszewski 
# emma.fitzgerald@eit.lth.se, artur@tele.pw.edu.pl
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

set arcs;

set streams;

param arc_used {streams, arcs} binary default 0;

set arborescence {s in streams} = {a in arcs : arc_used[s, a] = 1};
set all_arborescences = union{s in streams} arborescence[s];

param num_streams {a in all_arborescences} = sum{s in streams} arc_used[s, a]; 

set c_sets;
set c_set_arcs {c_sets} within all_arborescences;

var pi {a in all_arborescences} binary;

maximize dual_objective:
    sum{a in all_arborescences} (pi[a] * num_streams[a]);

subject to dual_constraint {c in c_sets}:
    sum{a in c_set_arcs[c]} pi[a] <= 1;
