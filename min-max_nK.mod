# Min-max energy optimisation for nK aggregation
#
# (C) Copyright 2018 Emma Fitzgerald, Artur Tomaszewski 
# emma.fitzgerald@eit.lth.se, artur@tele.pw.edu.pl
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

## Parameters ========================

param sm_Null symbolic := 'null';
param sm_Eps := 1e-6;

param sm_temp, symbolic;

# parameters

param sm_areaWidth, >= 0;
param sm_areaHeight, >= 0;

param sm_aggregationCost, >= 0, default 1;
param sm_transmissionCost, >= 0, default 1;
param sm_txCost = 0.6 * sm_transmissionCost;
param sm_rxCost = 0.4 * sm_transmissionCost;

param sm_K, integer, >= 1;

# nodes

set sm_Nodes ordered default {};
param sm_node_index { sm_Nodes }, integer, >= 0;
param sm_node_x { sm_Nodes };
param sm_node_y { sm_Nodes };

set sm_Sensors within sm_Nodes;
set sm_Aggregators within sm_Nodes;
set sm_Processors within sm_Nodes;

# links

set sm_Links ordered;
param sm_link_nodeA { sm_Links }, symbolic, in sm_Nodes;
param sm_link_nodeB { sm_Links }, symbolic, in sm_Nodes;
param sm_link_power { sm_Links }, >= 0;
param sm_link_tx_power {l in sm_Links} = 0.6 * sm_link_power[l];
param sm_link_rx_power {l in sm_Links} = 0.4 * sm_link_power[l];


## Variables ========================

var sm_sensor_measurementProcessed { s in sm_Sensors, p in sm_Processors }, >= 0, <= 1;
var sm_sensor_anyMeasurementProcessed {s in sm_Sensors}, binary;
var sm_link_measurementFlow { e in sm_Links, s in sm_Sensors, p in (sm_Sensors diff {s}) union sm_Processors }, >= 0, <= 1, binary;
var sm_link_used { e in sm_Links }, >= 0, <= 1, binary;
var sm_node_tx_power { v in sm_Nodes }, >= 0;
var sm_aggregator_aggregationCount { a in sm_Sensors union sm_Aggregators }, >= 0;

var sm_link_usedBySensor { e in sm_Links, s in sm_Sensors }, >= 0, <= 1;
var sm_node_measurementsAggregated { v in sm_Sensors union sm_Aggregators union sm_Processors, s1 in sm_Sensors, s2 in sm_Sensors: s1 <> s2 }, >= 0, <= 1;

var sm_max_energy >= 0;
var sm_node_energy {sm_Sensors union sm_aggregators}y >= 0;

## Constraints ========================

subject to sm_MeasurementsProcessed { p in sm_Processors }:
  sum { s in sm_Sensors } sm_sensor_measurementProcessed[ s, p ] >= sm_K;

subject to sm_AnyMeasurementProcessed {s in sm_Sensors, p in sm_Processors}:
  sm_sensor_anyMeasurementProcessed[s] >= sm_sensor_measurementProcessed[s, p];

subject to sm_MeasurementSent { s in sm_Sensors, p in sm_Processors }:
  sum { e in sm_Links: sm_link_nodeA[ e ] = s } sm_link_measurementFlow[ e, s, p ] = sm_sensor_measurementProcessed[ s, p ];

subject to sm_MeasurementReceived { s in sm_Sensors, p in sm_Processors }:
  sum { e in sm_Links: sm_link_nodeB[ e ] = p } sm_link_measurementFlow[ e, s, p ] = sm_sensor_measurementProcessed[ s, p ];

subject to sm_MeasurementFlows { s in sm_Sensors, p in (sm_Sensors diff {s}) union sm_Processors, v in sm_Nodes: v <> s and v <> p }:
  sum { e in sm_Links: sm_link_nodeA[ e ] = v } sm_link_measurementFlow[ e, s, p ] - sum { e in sm_Links: sm_link_nodeB[ e ] = v } sm_link_measurementFlow[ e, s, p ] = 0;

subject to sm_NoMeasurementFlow { s in sm_Sensors, p in sm_Processors, e in sm_Links: sm_link_nodeA[ e ] in sm_Sensors and sm_link_nodeA[ e ] <> s or sm_link_nodeB[ e ] in sm_Processors and sm_link_nodeB[ e ] <> p }:
  sm_link_measurementFlow[ e, s, p ] = 0;

subject to sm_LinkUsed { e in sm_Links, s in sm_Sensors, p in sm_Processors }:
  sm_link_used[ e ] >= sm_link_measurementFlow[ e, s, p ];

subject to sm_NodePower { v in sm_Sensors union sm_Aggregators, e in sm_Links: sm_link_nodeA[ e ] = v }:
  sm_node_tx_power[ v ] >= sm_link_tx_power[ e ] * sm_link_used[ e ];

subject to sm_AggregationCount { a in sm_Sensors union sm_Aggregators }:
  sm_aggregator_aggregationCount[ a ] >= sum { e in sm_Links: sm_link_nodeB[ e ] = a } sm_link_used[ e ] - 1;

subject to sm_AggregationCountSensors { a in sm_Sensors }:
  sm_aggregator_aggregationCount[ a ] >= sum { e in sm_Links: sm_link_nodeB[ e ] = a } sm_link_used[ e ] + sm_sensor_anyMeasurementProcessed[a] - 1;

subject to sm_NodeEnergy {v in sm_Sensors union sm_Aggregators }:
    sm_node_energy = sm_tx_cost * sm_node_tx_power + sm_rx_cost * sum{e in sm_Links: sm_link_nodeA[ e ] = v} (sm_link_rx_power[e] * sm_link_used[e]) + sm_aggregationCost * sm_aggregator_aggregationCount[v]; 

subject to sm_MostEnergy {v in sm_Sensors union sm_Aggregators}:
    sm_max_energy >= sm_node_energy[v];


subject to sm_LinkUsedBySensor { e in sm_Links, s in sm_Sensors, p in sm_Processors }:
  sm_link_usedBySensor[ e, s ] >= sm_link_measurementFlow[ e, s, p ];

subject to sm_NoSensorAggregation { s in sm_Sensors, a in sm_Sensors union sm_Aggregators }:
  sum { e in sm_Links: sm_link_nodeB[ e ] = a } sm_link_usedBySensor[ e, s ] <= 1;

subject to sm_MeasurementsAggregated { v in sm_Sensors union sm_Aggregators union sm_Processors, s1 in sm_Sensors, s2 in sm_Sensors, e1 in sm_Links, e2 in sm_Links: s1 <> s2 and sm_link_nodeB[ e1 ] = v and sm_link_nodeB[ e2 ] = v and e1 <> e2 }:
  sm_node_measurementsAggregated[ v, s1, s2 ] >= sm_link_usedBySensor[ e1, s1 ] + sm_link_usedBySensor[ e2, s2 ] - 1;

subject to sm_NoDoubleMeasurementsAggregation { s1 in sm_Sensors, s2 in sm_Sensors: s1 <> s2 }:
  sum { v in sm_Sensors union sm_Aggregators union sm_Processors } sm_node_measurementsAggregated[ v, s1, s2 ] <= 2;

subject to sm_NoLinkUseWithoutFlow {s in sm_Sensors, e in sm_Links }:
  sm_link_usedBySensor[ e, s ] <= sum { p in sm_Processors } sm_link_measurementFlow[ e, s, p ];

## Objective ========================

minimize Max_energy:
    sm_max_energy;


## Problem ========================

problem sm_MeasurementsAggregation:
  sm_sensor_measurementProcessed, sm_MeasurementsProcessed,
  sm_link_measurementFlow, sm_MeasurementSent, sm_MeasurementReceived, sm_MeasurementFlows, sm_NoMeasurementFlow,
  sm_link_used, sm_LinkUsed,
  sm_node_power, sm_NodePower,
  sm_aggregator_aggregationCount, sm_AggregationCount,
  sm_link_usedBySensor, sm_LinkUsedBySensor, sm_NoSensorAggregation,
  sm_node_measurementsAggregated, sm_MeasurementsAggregated, sm_NoDoubleMeasurementsAggregation,
  sm_TotalPower
;
