#!/usr/bin/python

# Generate a network scenario at random and output it as an AMPL data file
#
# (C) Copyright 2018 Emma Fitzgerald, Artur Tomaszewski 
# emma.fitzgerald@eit.lth.se, artur@tele.pw.edu.pl
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import random
import os
import sys
import numpy
from numpy.random import seed as npseed
from numpy.random import poisson
import pygraphviz as pgv
import copy
import time
import subprocess

# Parameters:
#   Number of nodes
num_nodes = 100
num_origins = 15
num_destinations = 10
# Streams
num_streams = 1
destinations_per_stream = 5
origins_per_stream = 15
packets_needed_oneK = 8
packets_needed_nK = 4
#   Area to distribute nodes X and Y
playground_width = 400.0   # metres
playground_height = 400.0  # metres
#   Distribution to use for node placement and distribution params
location_distribution = "Uniform"
location_lambda = 10.0  # nodes per square metre
#   Channel model, channel model params
#transmission_range = 1.0  # metres
noise = -81.0    # dBm
path_loss_exponent = 4
transmit_power_dist = 0.1
transmission_power_mW = 100
transmission_power = 20.0     # dBm
SINR_thresh = 8.0      # dB
# Energy costs
trans_energy = 5
aggr_energy = 1

# Graph scale
SCALE = 4

# Class to represent a node
class Node:
    def __init__(self, name = "", x = 0, y = 0, id = 0, role = "aggregator"):
        self.name = name
        self.x = x
        self.y = y
        self.packets = []
        self.role = role
        self.transmissions = []
        self.neighbours = {}
        self.latest_time = 0
        self.hops = 0
        self.id = id
        self.demands = []
        self.role = role
        self.set_rolestr()

    def set_rolestr(self):
        if self.role == "sink":
            self.rolestr = "s"
        elif self.role == "origin":
            self.rolestr = "o"
        elif self.role == "aggregator":
            self.rolestr = "a"
        elif self.role == "destination":
            self.rolestr = "d"
        else:
            self.rolestr = "?"

    def set_role(self, role):
        self.role = role
        self.set_rolestr()

    def set_location(self, x, y):
        self.x = x
        self.y = y

    def queue_packets(self, new_packets):
        self.packets.extend(new_packets)

    def set_name(self, name):
        self.name = name

    def add_neighbour(self, node):
        self.neighbours[node] = 1

    def __repr__(self):
        packets_dict = self.packets_to_receivers()
        packet_string = ""
        for i in sorted(packets_dict.keys()):
            packet_string += i.name + ": " + str(len(packets_dict[i])) + ", "
        return self.name + ": (" + str(self.x) + ", " + str(self.y) + ")" + self.rolestr

    def packets_to_receivers(self):
        packet_dict = {}
        for packet in self.packets:
            receiver = packet[0]
            if receiver not in packet_dict.keys():
                packet_dict[receiver] = []
            packet_dict[receiver].append(packet[1])

        return packet_dict

class Stream:
    def __init__(self, id=0):
        self.id = id
        self.origins = []
        self.destinations = []

    def add_origins(self, origins):
        self.origins.extend(origins)
    
    def add_destinations(self, dests):
        self.destinations.extend(dests)

class C_set:
    def __init__(self, id=0):
        self.id = id
        self.nodes = []
        self.links = []

    def add_nodes(self, nodes):
        self.nodes.extend(nodes)

    def add_links(self, links):
        self.links.extend(links)

# Create a list of nodes
def create_nodes(num):
    nodes = []
    for i in xrange(num):
        letter = i % 26
        nodename = str(chr(letter + ord('A')))
        letter = int(i / 26)
        nodename = str(chr(letter + ord('A'))) + nodename
        if i >= 26 and i < 52:
            nodename = "A" + nodename 
        node = Node(name = nodename, id = i)

        nodes.append(node)
    return nodes

# Determine locations for a list of nodes
def place_nodes(nodes, width, height, distribution, dist_params):
    circles = []
    for node in nodes:
        while True:
            # Generate a location within the playground
            x, y = generate_location(width, height, distribution, dist_params)

            if node.role == "destination" or node.role == "origin" or node.role == "sink":
                break

            for circle in circles:
                dist = math.sqrt(math.pow(circle[0] - x, 2) + math.pow(circle[1] - y, 2))
                if dist < 20:
                    continue
            break

        circles.append([x, y])

#        if node.role == "destination":
#            y = height - 20
#        elif node.role == "origin":
#            y = 10
#        else:
#            y += 10

        # Assign the location to the node
        node.set_location(x, y)

# Generate a location for a node according to a given distribution
def generate_location(width, height, distribution, dist_params):
    x = 0
    y = 0
    if distribution == "Uniform":
        x = rng.uniform(0.0, width)
        y = rng.uniform(0.0, height)

    return [x, y]

# Determine which links interfere with each other
def get_interfering_links(links):
    # 2D array of interference: interference[i][j] == True indicates link
    # i interferes with link j, and correspondingly link j is interfered by
    # link i
    interferes_with = {}
    interfered_by = {}

    for l in links:
        interferes_with[l] = [] 
        interfered_by[l] = []

    for l in links:
        for m in links:
            if l == m:
                continue
            if interferes(l, m) or l[0] == m[0] or l[1] == m[0] or m[1] == l[0]:
                interferes_with[l].append(m)
                interfered_by[m].append(l)

    return [interferes_with, interfered_by]

# Get list of links between nodes based on node locations
def get_links(nodes):
    links = []
    for i in nodes:
        for j in nodes:
            # Origins do not have incoming links
            #if j.role == "origin":
            #    continue
            # Outgoing links from destinations only to the sink
            if i.role == "destination" and j.role != "sink":
                continue
            # Don't connect origins directly to destinations
            if i.role == "origin" and j.role == "destination":
                continue

            if within_range(j, i) and i != j:
                links.append((i, j))

    for node in nodes:
        for l in links:
            if l[0] == node:
                node.add_neighbour(l[1])

    return links

# Returns True if node n1 is within transmission range of node n2, False
# otherwise
def within_range(n1, n2):
    d = distance(n1, n2)
    if d == 0:
        return True
    SNR = transmission_power - (path_loss_exponent * 10 * math.log10(d)) - 20 - noise
    return SNR >= SINR_thresh

# Returns the Cartesian distance between two nodes
def distance(n1, n2):
    return math.sqrt(math.pow(n1.x - n2.x, 2) + math.pow(n1.y - n2.y, 2))

# Returns True if l1 interferes with l2, False otherwise
def interferes(l1, l2):
    # Return true if receiver of l2 is within range of sender of l1
    if(within_range(l2[1], l1[0])):
        return True
    return False

def generate_streams(nodes):
    origins = []
    destinations = []
    streams = []

    for node in nodes:
        if node.role == "origin":
            origins.append(node)
        if node.role == "destination":
            destinations.append(node)

    for i in xrange(num_streams):
        s = Stream(i)
        s.add_origins(random.sample(origins, origins_per_stream))
        s.add_destinations(random.sample(destinations, destinations_per_stream))
        streams.append(s)

    return streams

# Create a network scenario with nodes, links between them, traffic at each
# node and interference between links
def generate_mesh_network():
    nodes = create_nodes(num_nodes)

    for i in xrange(num_origins):
        nodes[i].set_role("origin")

    for i in xrange(num_origins, num_origins + num_destinations):
        nodes[i].set_role("destination")

    place_nodes(nodes, playground_width, playground_height, 
        location_distribution, [location_lambda])
    links = get_links(nodes)
    interference = get_interfering_links(links)

    sink = Node("Sink", id=num_nodes, role="sink")
    sink.set_location(playground_width/2.0, playground_height/2.0)
    nodes.append(sink)
    for node in nodes:
        if node.role == "destination":
            links.append([node,sink])

    for node in nodes:
        print node
    
    streams = generate_streams(nodes)

    return [nodes, streams, links, interference]

def output_network(nodes, streams, links, interference, seed, suffix):
    f = open("scenario" + suffix, "w")
    
    f.write("Seed:\n")
    f.write(str(seed))
    f.write("\n")

    f.write("\nNodes:\n")
    for n in nodes:
        f.write(str(n) + "\n")

    f.write("\nLinks:\n")
    for l in links:
        f.write(l[0].name + "\t" + l[1].name + "\n")

    f.write("\nStreams:\n")
    for s in streams:
        f.write("ID: " + str(s.id))
        f.write("\nOrigins: ")
        for o in s.origins:
            f.write(str(o.id) + " ")
        f.write("\nDestinations: ")
        for d in s.destinations:
            f.write(str(d.id) + " ")
        f.write("\n")

    f.close()

# Get average and confidence interval delta of a vector of values
def process_vector(v):
    if len(v) == 0:
        print "Empty vector"
        return (-1, -1)
    avg = numpy.mean(v)
    stddev = numpy.std(v)
    n = len(v)
    delta = 1.96 * (stddev / math.sqrt(n)) 
    return (avg, delta)

def process_results(results):
    result_string = ""
    for vec in results:
        avg, delta = process_vector(vec)
        result_string += "\t" + str(avg) + "\t" + str(delta)
    return result_string

def draw_network(nodes, streams, links, filename):
    os.chdir("scenarios")
    g = pgv.AGraph(directed=True, overlap=True, splines=True, bgcolor="white", dpi=384)
    # scale to make visual layout better
    scale = SCALE 
    # Dummy nodes for layout - ensures graph output uses the whole canvas
    g.add_node("dummy_a", label="", pos="0,0", color="transparent", fixedsize=True, width=0)
    br = str(playground_width*scale) + "," + str(playground_height*scale)
    g.add_node("dummy_b", label="", pos=br, color="transparent", fixedsize=True, width=0)

    for node in nodes:
        x = node.x
        y = node.y
        loc = str(x*scale) + "," + str(y*scale) + "!"
        name = str(node.id)
        if node.role == "sink":
            colour = "#FFFFAA"
        elif node.role == "origin":
            colour = "#FFAAAA"
        elif node.role == "destination":
            colour = "#AAAAFF"
        else:
            colour = "white"
        g.add_node(name, pos=loc, shape="circle", fillcolor=colour, pin=True, width=0.4, \
                    style="filled", fixedsize=True)
    linkdict = {}
    for link in links:
        start = str(link[0].id)
        end = str(link[1].id)
        #if start+end not in linkdict and end+start not in linkdict:
        #    linkdict[start+end] = 1
        g.add_edge(start, end, color="black")
    g.layout(prog="neato", args="-n")
    g.draw(filename)
    
    os.chdir("..")
    return g

def output_routing_data_oneK(nodes, streams, links, run, seed):
    os.chdir("routing_data_oneK")

    filename = "scenario" + str(len(nodes)) + "_nodes_" + str(run) + "_network.dat"

    network_file = open(filename, "w")
    network_file.write("data;\n\n")
    network_file.write("# SEED: " + str(seed) + "\n\n")
    network_file.write("param area_width := " + str(playground_width) + "; /* [m] */\n")
    network_file.write("param area_height := " + str(playground_height) + "; /* [m] */\n")

    network_file.write("param trans_energy := " + str(trans_energy) + ";\n")
    network_file.write("param aggr_energy := " + str(aggr_energy) + ";\n\n")

    network_file.write("# Nodes\n\n")
    
    # Output node sets
    network_file.write("set sensors := \n")
    for i in xrange(num_origins):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("set gateways := \n")
    for i in xrange(num_origins, num_origins + num_destinations):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("set aggregators := \n")
    for i in xrange(num_origins + num_destinations, num_nodes):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("param sink := " + str(nodes[-1].id) + ";\n\n")

    network_file.write("param: node_x node_y :=\n")
    for node in nodes:
        network_file.write(str(node.id) + "\t" + str(node.x) + "\t" + str(node.y) + "\n")
    network_file.write(";\n\n")

    network_file.write("# Arcs\n\n")
    network_file.write("param: arcs: arc_start arc_end :=\n")
    for link in links:
        network_file.write(str(link[0].id) + "_" + str(link[1].id) + "\t")
        network_file.write(str(link[0].id) + "\t" + str(link[1].id) + "\n")
        #network_file.write(str(distance(link[0], link[1])) + "\n")
    network_file.write(";\n\n")

    network_file.write("# Streams\n\n")

    network_file.write("param: streams: packets_needed :=\n")
    for stream in streams:
        network_file.write(str(stream.id) + "\t")
        network_file.write(str(packets_needed_oneK) + "\n")
    network_file.write(";\n\n")

    for stream in streams:
        network_file.write("set origins[" + str(stream.id) + "] := ")
        for origin in stream.origins:
             network_file.write(str(origin.id) + " ")
        network_file.write(";\n")
    network_file.write("\n")

    for stream in streams:
        network_file.write("set destinations[" + str(stream.id) + "] := ")
        for dest in stream.destinations:
             network_file.write(str(dest.id) + " ")
        network_file.write(";\n")
    network_file.write("\n")

    network_file.close()
    os.chdir("..")

    filename = "routing_data_oneK/" + filename
    return filename

def output_routing_data_nK(nodes, streams, links, run, seed):
    os.chdir("routing_data_nK")

    filename = "scenario" + str(len(nodes)) + "_nodes_" + str(run) + "_network.dat"

    network_file = open(filename, "w")
    network_file.write("data;\n\n")
    network_file.write("# SEED: " + str(seed) + "\n\n")
    network_file.write("param sm_areaWidth := " + str(playground_width) + "; /* [m] */\n")
    network_file.write("param sm_areaHeight := " + str(playground_height) + "; /* [m] */\n")

    network_file.write("param sm_transmissionCost := " + str(trans_energy) + ";\n")
    network_file.write("param sm_aggregationCost := " + str(aggr_energy) + ";\n\n")

    network_file.write("param sm_K := " + str(packets_needed_nK) + ";\n\n")

    network_file.write("# Nodes\n\n")
    
    # Output node sets
    network_file.write("set sm_Nodes := \n")
    for node in nodes:
        # No sink in nK formulation
        if node.role == "sink":
            continue
        network_file.write(str(node.id) + "\n")
    network_file.write(";\n\n")

    network_file.write("set sm_Sensors := \n")
    for i in xrange(num_origins):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("set sm_Processors := \n")
    for i in xrange(num_origins, num_origins + num_destinations):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("set sm_Aggregators := \n")
    for i in xrange(num_origins + num_destinations, num_nodes):
        network_file.write(str(nodes[i].id) + "\n")
    network_file.write(";\n\n")

    network_file.write("param: sm_node_index sm_node_x sm_node_y :=\n")
    for node in nodes:
        network_file.write(str(node.id) + "\t" + str(node.id) + "\t" + str(node.x) + "\t" + str(node.y) + "\n")
    network_file.write(";\n\n")

    network_file.write("# Arcs\n\n")
    network_file.write("param: sm_Links: sm_link_nodeA sm_link_nodeB sm_link_power :=\n")
    for link in links:
        if link[0].role == "sink" or link[1].role == "sink":
            continue
        network_file.write(str(link[0].id) + "_" + str(link[1].id) + "\t")
        network_file.write(str(link[0].id) + "\t" + str(link[1].id) + "\t")
        network_file.write("1\n")
    network_file.write(";\n\n")

    network_file.close()
    os.chdir("..")

    filename = "routing_data_nK/" + filename
    return filename

def solve_routing_problem_oneK(data_path):
    data_basename = os.path.basename(data_path)
    scenario = os.path.splitext(data_basename)[0]
    output_path = "routing_solutions_oneK/" + scenario + ".out"

    p = subprocess.Popen(["time", "-f %U %S", "ampl", "oneK.mod", data_path, "oneK.run"],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(output_path, "w")
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    ttime = -1.0
    for line in p.stderr:
        pieces = line.split()
        try:
            ttime = float(pieces[0]) + float(pieces[1])
        except:
            print data_path
            print line
            continue
    print ttime

    return output_path, ttime

def solve_routing_problem_nK(data_path):
    data_basename = os.path.basename(data_path)
    scenario = os.path.splitext(data_basename)[0]
    output_path = "routing_solutions_nK/" + scenario + ".out"

    p = subprocess.Popen(["time", "-f %U %S", "ampl", "nK.mod", data_path, "nK.run"],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(output_path, "w")
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    ttime = -1.0
    for line in p.stderr:
        pieces = line.split()
        try:
            ttime = float(pieces[0]) + float(pieces[1])
        except:
            print data_path
            print line
            continue
    print ttime

    return output_path, ttime

def graph_solution(solution, graph, links, nodes, streams):
    colour = "#66CC66"

    for stream in streams:
        graph_file = os.path.splitext(solution)[0] + "_" + str(stream.id) + ".png"
        g = graph.copy()

        if stream.id in nodes:
            for node_name in nodes[stream.id]:
                node = g.get_node(node_name)
                node.attr["fillcolor"] = colour

        if stream.id in links:
            for link in links[stream.id]:
                start = g.get_node(link[0])
                end = g.get_node(link[1])

                edge = g.get_edge(start, end)
                edge.attr["color"] = colour
                edge.attr["style"] = "bold"
                edge.attr["weight"] = 6.0 

        g.layout(prog="neato", args="-n")
        g.draw(graph_file)

def parse_routing_solution_oneK(solution, sink_id):
    solution_file = open(solution, "r")
    
    links = {}
    nodes = {}

    objective = -1
    trans_energy_used = -1
    aggr_energy_used = -1

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = int(line.split()[-1])
        if "total_trans_energy" in line:
            trans_energy_used = int(line.split()[-1])
        if "total_aggr_energy" in line:
            aggr_energy_used = int(line.split()[-1])
        if line.startswith("arc_used"):
            state = "ARCS"
            continue
        if (state == "ARCS" or state == "NODES") and line.startswith(";"):
            state = "START"
            continue
        if state == "ARCS":
            pieces = line.split()
            if pieces[2] == "1":
                # Arc is used by a stream
                stream_id = int(pieces[0])
                link_name = pieces[1]
                start, end = link_name.split("_")
                
                if end != sink_id:
                    if stream_id not in links:
                        links[stream_id] = []
                    links[stream_id].append([start, end])
            continue
        if line.startswith("packets_aggregated"):
            state = "NODES"
            continue
        if state == "NODES":
            pieces = line.split()
            if pieces[2] != "0":
                # Node aggregated some packets
                stream_id = int(pieces[0])
                node_name = pieces[1]

                if node_name != sink_id:
                    if stream_id not in nodes:
                        nodes[stream_id] = []
                    nodes[stream_id].append(node_name)

    aggr_nodes = {}
    for s in streams:
        if s.id in nodes:
            for n in nodes[s.id]:
                if n not in aggr_nodes:
                    aggr_nodes[n] = 1
    num_aggr_nodes = len(aggr_nodes.keys())

    return (objective, links, nodes, trans_energy_used, aggr_energy_used, num_aggr_nodes)

def parse_routing_solution_nK(solution, sink_id):
    solution_file = open(solution, "r")
    
    links = {}
    nodes = {}

    objective = -1
    trans_energy_used = -1
    aggr_energy_used = -1

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = int(line.split()[-1])
        if "total_trans_energy" in line:
            trans_energy_used = int(line.split()[-1])
        if "total_aggr_energy" in line:
            aggr_energy_used = int(line.split()[-1])
        if line.startswith("sm_link_used ["):
            state = "ARCS"
            continue
        if (state == "ARCS" or state == "NODES") and line.startswith(";"):
            state = "START"
            continue
        if state == "ARCS":
            pieces = line.split()
            if pieces[1] != "0":
                # Arc is used by a stream
                stream_id = 0
                link_name = pieces[0]
                start, end = link_name.split("_")
                
                if end != sink_id:
                    if stream_id not in links:
                        links[stream_id] = []
                    links[stream_id].append([start, end])
            continue
        if line.startswith("sm_aggregator_aggregationCount"):
            state = "NODES"
            continue
        if state == "NODES":
            pieces = line.split()
            if pieces[1] != "0":
                # Node aggregated some packets
                stream_id = 0
                node_name = pieces[0]

                if node_name != sink_id:
                    if stream_id not in nodes:
                        nodes[stream_id] = []
                    nodes[stream_id].append(node_name)

    aggr_nodes = {}
    for s in streams:
        if s.id in nodes:
            for n in nodes[s.id]:
                if n not in aggr_nodes:
                    aggr_nodes[n] = 1
    num_aggr_nodes = len(aggr_nodes.keys())

    return (objective, links, nodes, trans_energy_used, aggr_energy_used, num_aggr_nodes)

def generate_c_sets(nodes, links, sol_links):
    c_sets = []

    id_num = 0
    for node in nodes:
        c = C_set(id=id_num)

        c.add_nodes([node])
        c_links = []
        for link in links:
            if link[0] == node:
                # Only add links in arborescences to c-sets
                for stream in streams:
                    if [str(link[0].id), str(link[1].id)] in sol_links[stream.id]:
                        c_links.append(link)
                        break
        c.add_links(c_links)
        
        if len(c.links) > 0:
            c_sets.append(c)
            id_num += 1

    return c_sets

def output_frame_min_data(scenario, iteration, links, streams, c_sets, sol_links):
    os.chdir("frame_min_data")

    filename = "frame_min_" + scenario + "_iter_" + iteration + ".dat"

    frame_min_file = open(filename, "w")
    frame_min_file.write("data;\n\n")
    frame_min_file.write("# Scenario: " + scenario + ", iteration " + iteration + "\n\n")

    frame_min_file.write("# Streams\n\n")

    frame_min_file.write("set streams :=\n")
    for stream in streams:
        frame_min_file.write(str(stream.id) + "\n")
    frame_min_file.write(";\n\n")

    frame_min_file.write("# Arcs\n\n")
    frame_min_file.write("set arcs :=\n")
    for link in links:
        frame_min_file.write(str(link[0].id) + "_" + str(link[1].id) + "\n")
    frame_min_file.write(";\n\n")

    frame_min_file.write("param: arc_used :=\n")
    for stream in streams:
        for link in sol_links[stream.id]:
            frame_min_file.write(str(stream.id) + " " + str(link[0]) + "_" + str(link[1]) + " 1\n")
    frame_min_file.write(";\n\n")

    frame_min_file.write("# C-sets\n\n")
    frame_min_file.write("set c_sets :=\n")
    for c in c_sets:
        frame_min_file.write(str(c.id) + "\n")
    frame_min_file.write(";\n\n")

    for c in c_sets:
        frame_min_file.write("set c_set_arcs[" + str(c.id) + "] := ")
        for link in c.links:
                    frame_min_file.write(str(link[0].id) + "_" + str(link[1].id) + " ")
        frame_min_file.write(";\n\n")
    
    frame_min_file.close()
    os.chdir("..")

    filename = "frame_min_data/" + filename
    return filename

def solve_frame_min(scenario, iteration, frame_min_file):
    solution_file = "frame_min_solutions/" + "frame_min_" + scenario + "_iter_" + iteration + ".out"

    p = subprocess.Popen(["time", "-f %U %S", "ampl", "frame_min.mod", frame_min_file, "frame_min.run"],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(solution_file, "w")
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    for line in p.stderr:
        pieces = line.split()
        ttime = float(pieces[0]) + float(pieces[1])
    print ttime

    return solution_file, ttime

def parse_frame_min_solution(frame_min_solution):
    solution_file = open(frame_min_solution, "r")

    state = "START"
    pi_star = []
    for line in solution_file:
        if "objective" in line:
            frame_length = int(line.split()[-1])
            continue
        if state == "START" and line.startswith("pi"):
            state = "PI"
            continue
        if state == "PI" and line.startswith(";"):
            state = "START"
            continue
        if state == "PI":
            arc, val = line.split()
            if int(val) == 1:
                # pi for arc is 1
                pi_star.append(arc)

    return (frame_length, pi_star)

def output_pricing_data(scenario, iteration, nodes, links, streams, sol_nodes, sol_links, pi_star):
    os.chdir("pricing_data")

    filename = "pricing_" + scenario + "_iter_" + iteration + ".dat"

    pricing_file = open(filename, "w")
    pricing_file.write("data;\n\n")
    pricing_file.write("# Scenario: " + scenario + ", iteration: " + iteration + "\n\n")

    pricing_file.write("# Nodes\n\n")
    
    # Output node sets
    pricing_file.write("set nodes := \n")
    for node in nodes:
        pricing_file.write(str(node.id) + "\n")
    pricing_file.write(";\n\n")

    nodes_used = {}
    for stream in streams:
        for link in sol_links[stream.id]:
            nodes_used[link[0]] = 1
            nodes_used[link[1]] = 1

    pricing_file.write("set nodes_used := \n")
    for node in nodes_used:
            pricing_file.write(node + "\n")
    pricing_file.write(";\n\n")

    pricing_file.write("param: node_x node_y :=\n")
    for node in nodes:
        pricing_file.write(str(node.id) + "\t" + str(node.x) + "\t" + str(node.y) + "\n")
    pricing_file.write(";\n\n")

    pricing_file.write("# Arcs\n\n")
    pricing_file.write("param: arcs: arc_start arc_end :=\n")
    for link in links:
        pricing_file.write(str(link[0].id) + "_" + str(link[1].id) + "\t")
        pricing_file.write(str(link[0].id) + "\t" + str(link[1].id) + "\n")
    pricing_file.write(";\n\n")

    pricing_file.write("param: arc_used :=\n")
    for stream in streams:
        for link in sol_links[stream.id]:
            pricing_file.write(str(stream.id) + " " + str(link[0]) + "_" + str(link[1]) + " 1\n")
    pricing_file.write(";\n\n")

    pricing_file.write("# Streams\n\n")
    pricing_file.write("set streams :=\n")
    for stream in streams:
        pricing_file.write(str(stream.id) + "\n")
    pricing_file.write(";\n\n")

    pricing_file.write("# Dual variables\n\n")
    pricing_file.write("param: best_pi :=\n")
    for arc in pi_star:
        pricing_file.write(arc + " 1\n")
    pricing_file.write(";\n\n")

    pricing_file.close()
    os.chdir("..")

    filename = "pricing_data/" + filename
    return filename

def solve_pricing_problem(scenario, iteration, pricing_file):
    solution_file = "pricing_solutions/" + "pricing_" + scenario + "_iter_" + iteration + ".out"

    p = subprocess.Popen(["time", "-f %U %S", "ampl", "pricing.mod", pricing_file, "pricing.run", solution_file], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(solution_file, "w")
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    for line in p.stderr:
        pieces = line.split()
        ttime = float(pieces[0]) + float(pieces[1])
    print ttime

    return solution_file, ttime

def parse_pricing_solution(pricing_solution, next_id): 
    solution_file = open(pricing_solution, "r")

    state = "START"
    c = C_set(id=next_id)
    for line in solution_file:
        if "objective" in line:
            objective = int(line.split()[-1])
            continue
        if state == "START" and line.startswith("node_in_c_set"):
            state = "NODES"
            continue
        if state == "START" and line.startswith("arc_in_c_set"):
            state = "ARCS"
            continue
        if (state == "NODES" or state == "ARCS") and line.startswith(";"):
            state = "START"
            continue
        if state == "NODES":
            node_id = line.split()[0]

            # TODO: make this more efficient, maybe make nodes and links dictionaries instead of lists
            for node in nodes:
                if str(node.id) == node_id:
                    c.add_nodes([node])
                    break
            continue
        if state == "ARCS":
            arc = line.split()[0]
            start, end = arc.split("_")
            for link in links:
                if str(link[0].id) == start and str(link[1].id) == end:
                    c.add_links([link])
                    break
            continue

    return (objective, c)

def solve_primal_problem(scenario, frame_min_file):
    solution_file = "primal_solutions/" + "primal_" + scenario + ".out"

    p = subprocess.Popen(["time", "-f %U %S", "ampl", "primal.mod", frame_min_file, "primal.run"],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(solution_file, "w")
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    for line in p.stderr:
        pieces = line.split()
        ttime = float(pieces[0]) + float(pieces[1])
    print ttime

    return solution_file, ttime

def parse_primal_solution(primal_solution):
    solution_file = open(primal_solution, "r")

    print primal_solution
    state = "START"
    pi_star = []
    for line in solution_file:
        if "objective" in line:
            frame_length = float(line.split()[-1])
            break

    return frame_length

if __name__ == "__main__":
    if len(sys.argv) == 2: 
        base_seed = int(sys.argv[1])
    else:
        base_seed = random.randint(0, sys.maxint)

    global rng
    seed_rng = random.Random(base_seed)
    num_runs = 20

    results_file = open("results/" + str(num_nodes) + "_results.dat", "w")
    results_file.write("# Num nodes\tRun\tTotal energy 1K\tTotal energy nK\tTrans energy 1K\t")
    results_file.write("Trans energy nK\tAggr energy 1K\tAggr energy nK\tFrame length\t")
    results_file.write("Time routing 1K (s)\tTime routing nK (s)\tTime frame min (s)\tIter frame min\tNum c-sets\tNum aggr nodes 1K\tNum aggr nodes nK\n")

    aggr_results_file = open("aggregated_results.dat", "w")
    aggr_results_file.write("# Num nodes\tEnergy 1K\tdelta\tEnergy nK\tdelta\t")
    aggr_results_file.write("Trans energy 1K\tdelta\tTrans energy nK\tdelta\t")
    aggr_results_file.write("Aggr energy 1K\tdelta\tAggr energy nK\tdelta\tFrame length\tdelta\t")
    aggr_results_file.write("Routing time 1K\tdelta\tRouting time nK\tdelta\t")
    aggr_results_file.write("Frame min time\tdelta\tIterations\tdelta\tNum c-sets\tdelta\tNum aggr nodes 1K\tdelta\tNum aggr nodesnK\tdelta\n")

    for num_nodes in [10, 15, 20, 25, 30, 35, 40]:
        num_origins = int(math.ceil(num_nodes * 0.4))
        num_destinations = int(math.ceil(num_nodes * 0.15))
        num_streams = 1
        destinations_per_stream = num_destinations
        origins_per_stream = num_origins
        packets_needed_oneK = int(math.ceil(num_origins * 0.75))
        packets_needed_nK = packets_needed_oneK

        area = 1500.0 * num_nodes
        playground_width = math.sqrt(area)
        playground_height = math.sqrt(area)

        energy_used_oneK_vec = []
        trans_energy_used_oneK_vec = []
        aggr_energy_used_oneK_vec = []
        energy_used_nK_vec = []
        trans_energy_used_nK_vec = []
        aggr_energy_used_nK_vec = []
        frame_length_vec = []
        routing_time_oneK_vec = []
        routing_time_nK_vec = []
        frame_min_time_vec = []
        iterations_vec = []
        c_sets_vec = []
        aggr_nodes_oneK_vec = []
        aggr_nodes_nK_vec = []

        print "Nodes:", num_nodes
        print "Origins:", num_origins
        print "Destinations:", num_destinations
        print "Packets to collect, 1K:", packets_needed_oneK
        print "Packets to collect, nK:", packets_needed_nK

        for i in xrange(num_runs):
            print "Run:", i
            while True:
                print "Generating scenario..."
                sys.stdout.flush()

                received_map = {}

                seed = seed_rng.randint(0, sys.maxint)
                npseed(seed)
                rng = random.Random(seed)
                print "seed:", seed

                #num_nodes = poisson(location_lambda * playground_width * playground_height)
                print "Number of nodes:", num_nodes

                nodes = []
                streams = []
                links = []
                energy_used_oneK = 0
                trans_energy_used_oneK = 0
                aggr_energy_used_oneK = 0
                energy_used_nK = 0
                trans_energy_used_nK = 0
                aggr_energy_used_nK = 0

                nodes, streams, links, interference = generate_mesh_network()

                #output_network(nodes, links, interference, seed, str(i))
                graph = draw_network(nodes, streams, links, "graph" + str(num_nodes) + "nodes_" + str(i) + ".png")
                scenario_file_oneK = output_routing_data_oneK(nodes, streams, links, i, seed)
                scenario_file_nK = output_routing_data_nK(nodes, streams, links, i, seed)

                print "Created scenario"
                sys.stdout.flush()

                solution_file_oneK, routing_time_oneK = solve_routing_problem_oneK(scenario_file_oneK)
                sys.stdout.flush()
                solution_file_nK, routing_time_nK = solve_routing_problem_nK(scenario_file_nK)
                sys.stdout.flush()

                # Once we have solved the routing problem, do not use the sink and its incoming arcs any more
                sink = nodes[-1]
                nodes = nodes[:-1]

                all_links = copy.copy(links)
                links = []
                for link in all_links:
                    if link[1].id != sink.id:
                        links.append(link)

                energy_used_oneK, sol_links_oneK, sol_nodes_oneK, trans_energy_used_oneK, aggr_energy_used_oneK, aggr_nodes_oneK = \
                                                                    parse_routing_solution_oneK(solution_file_oneK, str(sink.id))
                energy_used_nK, sol_links_nK, sol_nodes_nK, trans_energy_used_nK, aggr_energy_used_nK, aggr_nodes_nK = \
                                                                    parse_routing_solution_nK(solution_file_nK, str(sink.id))

                if energy_used_oneK == -1 or energy_used_nK == -1:
                    # AMPL failed to find the objective. Most likely we generated an infeasible scenario, try again
                    print "Infeasible scenario, re-generating..."
                    sys.stdout.flush()
                    continue

                print "Solved routing for scenario, objective =", energy_used_oneK, "(1K),", energy_used_nK, "(nK)"
                sys.stdout.flush()

                graph_solution(solution_file_oneK, graph, sol_links_oneK, sol_nodes_oneK, streams)
                graph_solution(solution_file_nK, graph, sol_links_nK, sol_nodes_nK, streams)

                print "Drawn routing solutions"
                sys.stdout.flush()

                print "Solving frame minimisation problem"
                sys.stdout.flush()

                c_sets = generate_c_sets(nodes, links, sol_links_oneK)
                next_c_set_id = len(c_sets)
                iterations = 1
                frame_min_time = 0
                while(True):
                    print "Iteration " + str(iterations) + ":\n"
                    sys.stdout.flush()

                    scenario_string = "scenario" + "_" + str(num_nodes) + "_nodes_" + str(i)
                    frame_min_file = output_frame_min_data(scenario_string, str(iterations), links, streams, c_sets, sol_links_oneK)
                    frame_min_solution, fm_time = solve_frame_min(scenario_string, str(iterations), frame_min_file)
                    frame_min_time += fm_time
                    frame_length, pi_star = parse_frame_min_solution(frame_min_solution)
                    
                    print "Solved frame minimisation problem, objective =", frame_length
                    sys.stdout.flush()

                    pricing_file = output_pricing_data(scenario_string, str(iterations), 
                                                nodes, links, streams, sol_nodes_oneK, sol_links_oneK, pi_star)
                    pricing_solution, p_time = solve_pricing_problem(scenario_string, str(iterations), pricing_file)
                    frame_min_time += p_time
                    pricing_obj, c_set = parse_pricing_solution(pricing_solution, next_c_set_id)
                    next_c_set_id += 1

                    print "Solved pricing problem, objective =", pricing_obj 
                    sys.stdout.flush()

                    if pricing_obj > 1.0:
                        c_sets.append(c_set)
                    else:
                        print "Pricing problem solution <= 1; frame minimisation complete"
                        sys.stdout.flush()

                        print "Solving primal"
                        primal_solution, p_time = solve_primal_problem(scenario_string, frame_min_file)
                        frame_min_time += p_time
                        frame_length = parse_primal_solution(primal_solution)
                        break

                    iterations += 1

                break

            results_file.write(str(num_nodes) + "\t")
            results_file.write(str(i) + "\t")

            energy_used_oneK_vec.append(energy_used_oneK)
            results_file.write(str(energy_used_oneK) + "\t")

            energy_used_nK_vec.append(energy_used_nK)
            results_file.write(str(energy_used_nK) + "\t")

            trans_energy_used_oneK_vec.append(trans_energy_used_oneK)
            results_file.write(str(trans_energy_used_oneK) + "\t")

            trans_energy_used_nK_vec.append(trans_energy_used_nK)
            results_file.write(str(trans_energy_used_nK) + "\t")

            aggr_energy_used_oneK_vec.append(aggr_energy_used_oneK)
            results_file.write(str(aggr_energy_used_oneK) + "\t")

            aggr_energy_used_nK_vec.append(aggr_energy_used_nK)
            results_file.write(str(aggr_energy_used_nK) + "\t")

            frame_length_vec.append(frame_length)
            results_file.write(str(frame_length) + "\t")

            routing_time_oneK_vec.append(routing_time_oneK)
            results_file.write(str(routing_time_oneK) + "\t")

            routing_time_nK_vec.append(routing_time_nK)
            results_file.write(str(routing_time_nK) + "\t")

            frame_min_time_vec.append(frame_min_time)
            results_file.write(str(frame_min_time) + "\t")

            iterations_vec.append(iterations)
            results_file.write(str(iterations) + "\t")

            c_sets_vec.append(len(c_sets))
            results_file.write(str(len(c_sets)) + "\t")

            aggr_nodes_oneK_vec.append(aggr_nodes_oneK)
            results_file.write(str(aggr_nodes_oneK) + "\t")

            aggr_nodes_nK_vec.append(aggr_nodes_nK)
            results_file.write(str(aggr_nodes_nK) + "\n")

            results_file.flush()

        all_results = [energy_used_oneK_vec, energy_used_nK_vec, trans_energy_used_oneK_vec, trans_energy_used_nK_vec,
                        aggr_energy_used_oneK_vec, aggr_energy_used_nK_vec, frame_length_vec, 
                        routing_time_oneK_vec, routing_time_nK_vec, frame_min_time_vec, iterations_vec, c_sets_vec, aggr_nodes_oneK_vec, aggr_nodes_nK_vec]
        res_str = process_results(all_results)
        aggr_results_file.write(str(num_nodes) + res_str + "\n")
        aggr_results_file.flush()

results_file.close()
aggr_results_file.close()
print "Done."
