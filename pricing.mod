# Data aggregation pricing problem model 
#
# (C) Copyright 2018 Emma Fitzgerald, Artur Tomaszewski 
# emma.fitzgerald@eit.lth.se, artur@tele.pw.edu.pl
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
set nodes;
param node_x {nodes} >= 0;
param node_y {nodes} >= 0;

set streams;

# Arcs
set arcs;
param arc_start {arcs} in nodes;
param arc_end {arcs} in nodes;
param arc_length {a in arcs} = sqrt((node_x[arc_start[a]]**2.0) + (node_y[arc_end[a]]**2.0));

param arc_used {streams, arcs} binary default 0;

set arborescence {s in streams} = {a in arcs : arc_used[s, a] = 1};
set all_arborescences = union{s in streams} arborescence[s];
set delta_in {n in nodes} = {a in all_arborescences: arc_end[a] = n};
set delta_out {n in nodes} = {a in all_arborescences: arc_start[a] = n};
set total_delta {n in nodes} = delta_in[n] union delta_out[n];

# Set of nodes used in any arborescence
set nodes_used;

# Radio parameters
param noise default -81.0;		# Noise floor
param gamma default 8.0;		# SINR threshold
param path_loss_exp default 4.0;	# Path loss exponent
param trans_power default 20.0;		# Transmission power
param gain default 20.0;
# Power received at v when w transmits
param recv_power {a in arcs} = trans_power - (path_loss_exp * 10.0 * log10(arc_length[a])) - gain;

# Dual variables
param best_pi {a in arcs} binary default 0;

var arc_in_c_set {a in all_arborescences} binary;
var node_in_c_set {v in nodes_used} binary;

# Auxiliary variables for resolving bi-linearities
var Z {u in nodes_used, a in all_arborescences} >= 0;

maximize pricing_objective:
    sum{a in all_arborescences} best_pi[a] * arc_in_c_set[a];

subject to one_arc {v in nodes_used}:
    sum{a in delta_in[v]} arc_in_c_set[a] + sum{b in delta_out[v]} arc_in_c_set[b] <= 1;

subject to node_included_if_transmits {v in nodes_used}:
    sum{a in delta_out[v]} arc_in_c_set[a] = node_in_c_set[v];

subject to SINR {a in all_arborescences}:
    (1.0 / gamma) * recv_power[a] * arc_in_c_set[a] >= noise * arc_in_c_set[a] + 
	sum{v in nodes_used diff {arc_start[a], arc_end[a]}} (recv_power[a] * Z[v, a]);
	
subject to auxiliary1 {v in nodes_used, a in all_arborescences}:
    Z[v, a] <= node_in_c_set[v];

subject to auxiliary2 {v in nodes_used, a in all_arborescences}:
    Z[v, a] <= arc_in_c_set[a];

subject to auxiliary3 {v in nodes_used, a in all_arborescences}:
    Z[v, a] >= node_in_c_set[v] + arc_in_c_set[a] - 1;
